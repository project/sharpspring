<?php

/**
 * @file
 * Hooks provided by the sharpspring module.
 */

/**
 * @addtogroup hooks
 * @{
 */

use Drupal\Core\Access\AccessResult;

/**
 * Control sharpspring access.
 *
 * Modules may implement this hook if they want to disable sharpspring tracking
 * for some reasons.
 *
 * @return \Drupal\Core\Access\AccessResultInterface|bool|null
 *   The access result.
 */
function hook_sharpspring_access() {
  // Disable for frontpage.
  if (\Drupal::service('path.matcher')->isFrontPage()) {
    return AccessResult::forbidden();
  }
  return AccessResult::neutral();
}

/**
 * Alter results of sharpspring access check.
 */
function hook_sharpspring_access_alter(array &$results) {
  // Force disable for frontpage.
  if (\Drupal::service('path.matcher')->isFrontPage()) {
    $result = AccessResult::forbidden();
  }
  else {
    $result = AccessResult::neutral();
  }
  $results['my_module_check'] = $result;
}

/**
 * @} End of "addtogroup hooks".
 */
