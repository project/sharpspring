<?php

namespace Drupal\sharpspring;

/**
 * Interface for helper classes that check access to SharpSpring.
 *
 * @package Drupal\sharpspring
 */
interface SharpSpringAccessInterface {

  /**
   * Determines whether we add the script to page.
   *
   * @return bool
   *   Return TRUE if user can be tacked.
   */
  public function check(): bool;

}
