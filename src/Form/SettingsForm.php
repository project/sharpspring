<?php

namespace Drupal\sharpspring\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\ConfigTarget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines the SharpSpring settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'sharpspring_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'sharpspring.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?Request $request = NULL): array {
    $form['account'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Tracking Settings'),
    ];

    $form['account']['sharpspring_account'] = [
      '#type' => 'textfield',
      '#title' => t('Web Property ID'),
      '#size' => 15,
      '#maxlength' => 20,
      '#required' => TRUE,
      '#description' => $this->t('This ID is unique to each site you want to track separately, and is in the form of KOI-xxxxxxx.'),
      '#config_target' => new ConfigTarget(
        'sharpspring.settings',
        'sharpspring_account',
        fn($value) => str_replace(['–', '—', '−'], '-', $value),
        fn($value) => str_replace(['–', '—', '−'], '-', $value),
      ),
    ];

    $form['account']['sharpspring_domain'] = [
      '#type' => 'textfield',
      '#title' => t('Web Property Domain'),
      '#size' => 60,
      '#maxlength' => 60,
      '#required' => TRUE,
      '#description' => $this->t("This is the SharpSpring sub-domain to which tracking information is sent. It is defined in the tracking code as '_setDomain'. Omit the protocol and path: it should fit the following format: koi-xxxxxx.sharpspring.com"),
      '#config_target' => 'sharpspring.settings:sharpspring_domain',
    ];

    $form['account']['finding_values'] = [
      '#markup' => $this->t('For more information on how to find and set these values please see <a href="http://help.sharpspring.com/customer/portal/articles/1497453-how-to-insert-sharpspring-tracking-code-how-to-add-additional-sites" target="_blank">http://help.sharpspring.com/customer/portal/articles/1497453-how-to-insert-sharpspring-tracking-code-how-to-add-additional-sites</a>'),
    ];

    $api_url = Url::fromUri('https://app.sharpspring.com/settings/pubapi');
    $form['api'] = [
      '#type' => 'fieldset',
      '#title' => t('API Settings'),
      '#description' => $this->t('For advanced integrations, input your Account ID and Secret Key from the :url', [':url' => Link::fromTextAndUrl('SharpSpring Public API', $api_url)->toString()]),
    ];

    $form['api']['sharpspring_api_account_id'] = [
      '#title' => $this->t('Account ID'),
      '#type' => 'textfield',
      '#size' => 40,
      '#maxlength' => 40,
      '#config_target' => 'sharpspring.settings:sharpspring_api_account_id',
    ];

    $form['api']['sharpspring_api_secret_key'] = [
      '#title' => $this->t('Secret Key'),
      '#type' => 'textfield',
      '#size' => 40,
      '#maxlength' => 40,
      '#config_target' => 'sharpspring.settings:sharpspring_api_secret_key',
    ];

    // Visibility settings.
    $form['tracking']['page_display'] = [
      '#type' => 'details',
      '#title' => $this->t('Pages'),
      '#group' => 'tracking_scope',
      '#open' => TRUE,
    ];

    $description_args = [
      '%blog' => 'blog',
      '%blog-wildcard' => 'blog/*',
      '%front' => '<front>',
    ];
    $description = $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", $description_args);
    $title = $this->t('Pages');

    $form['tracking']['page_display']['visibility_pages'] = [
      '#type' => 'radios',
      '#title' => $this->t('Add tracking code to specific pages'),
      '#options' => [
        $this->t('Every page except the listed pages'),
        $this->t('The listed pages only'),
      ],
      '#config_target' => 'sharpspring.settings:visibility_pages',
    ];
    $form['tracking']['page_display']['pages'] = [
      '#type' => 'textarea',
      '#title' => $title,
      '#title_display' => 'invisible',
      '#description' => $description,
      '#rows' => 10,
      '#config_target' => new ConfigTarget(
        'sharpspring.settings',
        'pages',
        NULL,
        fn($value) => trim($value),
      ),
    ];

    // Render the role overview.
    $form['tracking']['role_display'] = [
      '#type' => 'details',
      '#title' => $this->t('Roles'),
      '#group' => 'tracking_scope',
      '#open' => TRUE,
    ];

    $form['tracking']['role_display']['visibility_roles'] = [
      '#type' => 'radios',
      '#title' => $this->t('Add tracking code for specific roles'),
      '#options' => [
        $this->t('Add to the selected roles only'),
        $this->t('Add to every role except the selected ones'),
      ],
      '#config_target' => 'sharpspring.settings:visibility_roles',
    ];
    $roles = Role::loadMultiple();
    $form['tracking']['role_display']['roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Roles'),
      '#options' => array_map(fn(RoleInterface $role) => $role->label(), $roles),
      '#description' => $this->t('If none of the roles are selected, all users will be tracked. If a user has any of the roles checked, that user will be tracked (or excluded, depending on the setting above).'),
      '#config_target' => new ConfigTarget(
        'sharpspring.settings',
        'roles',
        NULL,
        fn($value) => array_filter($value),
      ),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);

    $values = $form_state->getValues();

    // Verify that every path is prefixed with a slash.
    if ($values['visibility_pages'] != 2) {
      $pages = preg_split('/(\r\n?|\n)/', $values['pages']);
      foreach ($pages as $page) {
        if (strpos($page, '/') !== 0 && $page !== '<front>') {
          $form_state->setErrorByName(
            'pages',
            $this->t('Path "@page" not prefixed with slash.', ['@page' => $page]),
          );
          // Drupal forms show one error only.
          break;
        }
      }
    }
  }

}
