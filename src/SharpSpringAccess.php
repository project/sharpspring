<?php

namespace Drupal\sharpspring;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\path_alias\AliasManagerInterface;

/**
 * Class that checks if access to SharpSpring is allowed.
 *
 * @package Drupal\sharpspring
 */
class SharpSpringAccess implements SharpSpringAccessInterface {

  /**
   * Page match.
   *
   * @var bool
   */
  protected $pageMatch;

  /**
   * The SharpSpring settings config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected Config $config;

  /**
   * SharpSpringAccess constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module handler.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   * @param \Drupal\Core\Path\CurrentPathStack $currentPath
   *   Current path.
   * @param \Drupal\path_alias\AliasManagerInterface $aliasManager
   *   Alias manager.
   * @param \Drupal\Core\Path\PathMatcherInterface $pathMatcher
   *   Path matcher.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   Current user.
   */
  public function __construct(
    protected ModuleHandlerInterface $moduleHandler,
    ConfigFactoryInterface $configFactory,
    protected CurrentPathStack $currentPath,
    protected AliasManagerInterface $aliasManager,
    protected PathMatcherInterface $pathMatcher,
    protected AccountInterface $currentUser,
  ) {
    $this->config = $configFactory->get('sharpspring.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function check(): bool {
    $result = AccessResult::neutral()
      ->andIf($this->pathCheckResult())
      ->andIf($this->roleCheck());

    $access = [];
    $results = [];
    $hook = 'sharpspring_access';
    $this->moduleHandler->invokeAllWith($hook, function (callable $hook, string $module) use ($results, &$access) {
      $results[$module] = $hook();

      if (is_bool($results[$module])) {
        $access[$module] = $results[$module];
      }

      if ($results[$module] instanceof AccessResult) {
        $access[$module] = $results[$module];
      }
    });

    $this->moduleHandler->alter('sharpspring_access', $access);

    foreach ($access as $module_result) {
      if (is_bool($module_result)) {
        $result = $result->andIf(AccessResult::forbiddenIf(!$module_result));
      }
      elseif ($module_result instanceof AccessResult) {
        $result = $result->andIf($module_result);
      }
    }

    return !$result->isForbidden();
  }

  /**
   * Check path.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Path result.
   */
  protected function pathCheckResult(): AccessResultInterface {
    if (!isset($this->pageMatch)) {
      $visibility = $this->config->get('visibility_pages');
      $setting_pages = $this->config->get('pages');

      if (!$setting_pages) {
        $this->pageMatch = TRUE;
        return AccessResult::allowed();
      }

      $pages = mb_strtolower($setting_pages);
      if ($visibility < 2) {
        $path = $this->currentPath->getPath();
        $path_alias = mb_strtolower($this->aliasManager->getAliasByPath($path));
        $path_match = $this->pathMatcher->matchPath($path_alias, $pages);
        $alias_match = (($path != $path_alias) && $this->pathMatcher->matchPath($path, $pages));
        $this->pageMatch = $path_match || $alias_match;

        // When $visibility has a value of 0, the tracking code is displayed on
        // all pages except those listed in $pages. When set to 1, it
        // is displayed only on those pages listed in $pages.
        $this->pageMatch = !($visibility xor $this->pageMatch);
      }
      else {
        $this->pageMatch = FALSE;
      }
    }

    return AccessResult::forbiddenIf(!$this->pageMatch);
  }

  /**
   * Check user roles.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   User result.
   */
  protected function roleCheck(): AccessResultInterface {
    $visibility = $this->config->get('visibility_roles');
    $roles = array_filter($this->config->get('roles'));

    // If no roles are selected, allow access for all roles.
    if (empty($roles)) {
      return AccessResult::allowed();
    }

    $user_roles = $this->currentUser->getRoles();
    $user_has_selected_role = !empty(array_intersect($user_roles, array_keys($roles)));

    return AccessResult::forbiddenIf(($visibility === 0 && !$user_has_selected_role) || ($visibility === 1 && $user_has_selected_role));
  }

}
