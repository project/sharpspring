/**
 * @file
 * SharpSpring specific javascript.
 */

/**
 * Handles sharpspring response.
 *
 * @param resp
 *   response object returned from SharpSpring
 */
function ssDrupalCallback(resp) {
  let sendData = {};

  if (resp) {
    if (resp.contact) {
      sendData = resp.contact;
    } else if (resp.lead) {
      sendData = resp.lead;
    }
    drupalSettings.sharpspring.response = sendData;
    Drupal.attachBehaviors();
  }
}

(function (Drupal, window) {
  const sharpspring = drupalSettings.sharpspring || false;

  if (sharpspring) {
    const _ss = window._ss || [];
    _ss.push(['_setDomain', `https://${sharpspring.domain}/net`]);
    _ss.push(['_setAccount', sharpspring.id]);
    _ss.push(['_setResponseCallback', ssDrupalCallback]);
    _ss.push(['_trackPageView']);
    const ss = document.createElement('script');
    ss.type = 'text/javascript';
    ss.async = true;
    ss.src = `${document.location.protocol === 'https:' ? 'https://' : 'http://'}${sharpspring.domain}/client/ss.js`;
    const scr = document.getElementsByTagName('script')[0];
    scr.parentNode.insertBefore(ss, scr);

    window._ss = _ss;
  }

  Drupal.behaviors.sharpspringfns = {
    attach: (context, settings) => {
      const sharpspring = settings.sharpspring || false;
      // This allows additional modules to define their own JS handler functions.
      if (sharpspring && sharpspring.calledFuncs && sharpspring.response) {
        for (let n = 0; n < sharpspring.calledFuncs.length; n++) {
          const fn = sharpspring.calledFuncs[n];
          if (typeof fn === 'function') {
            // SharpSpring module handler functions should accept
            // a response object as a variable.
            fn(sharpspring.response);
          }
        }
      }
    },
  };
})(Drupal, window);
