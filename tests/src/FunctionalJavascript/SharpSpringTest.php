<?php

namespace Drupal\Tests\sharpspring\Functional;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Tests the SharpSpring module functionality.
 *
 * @group sharpspring
 */
class SharpSpringTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'sharpspring_test',
    'node',
  ];

  /**
   * The Drupal state.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Configure SharpSpring settings.
    $this->config('sharpspring.settings')
      ->set('sharpspring_account', 'KOI-1234567')
      ->set('sharpspring_domain', 'koi-123456.sharpspring.com')
      ->set('pages', '')
      ->save();

    $this->state = $this->container->get('state');
  }

  /**
   * Tests if SharpSpring JavaScript is correctly added to pages.
   */
  public function testJavaScriptInjection(): void {
    $this->drupalGet('<front>');

    // Check if SharpSpring JavaScript is present.
    $drupalSettings = $this->getDrupalSettings();
    $this->assertEquals('KOI-1234567', $drupalSettings['sharpspring']['id']);
    $this->assertEquals('koi-123456.sharpspring.com', $drupalSettings['sharpspring']['domain']);
    $this->assertSession()->responseContains($drupalSettings['sharpspring']['domain'] . '/client/ss.js');
  }

  /**
   * Tests access checks based on pages for SharpSpring tracking.
   */
  public function testPageAccess(): void {
    // Create nodes.
    $this->drupalCreateContentType(['type' => 'page']);
    $tracked_node = $this->drupalCreateNode(['type' => 'page']);
    $untracked_node = $this->drupalCreateNode(['type' => 'page']);

    // Configure SharpSpring to track only specific pages.
    $this->config('sharpspring.settings')
      ->set('visibility_pages', 1)
      ->set('pages', '/node/' . $tracked_node->id())
      ->save();

    // Check tracked page.
    $this->drupalGet('node/' . $tracked_node->id());
    $drupalSettings = $this->getDrupalSettings();
    $this->assertArrayHasKey('sharpspring', $drupalSettings);

    // Check untracked page.
    $this->drupalGet('node/' . $untracked_node->id());
    $drupalSettings = $this->getDrupalSettings();
    $this->assertArrayNotHasKey('sharpspring', $drupalSettings);

    $tracked_node = $this->drupalCreateNode(['type' => 'page']);
    $untracked_node = $this->drupalCreateNode(['type' => 'page']);

    // Configure SharpSpring to track all pages except the listed ones.
    $this->config('sharpspring.settings')
      ->set('visibility_pages', 0)
      ->set('pages', '/node/' . $untracked_node->id())
      ->save();

    // Check tracked page.
    $this->drupalGet('node/' . $tracked_node->id());
    $drupalSettings = $this->getDrupalSettings();
    $this->assertArrayHasKey('sharpspring', $drupalSettings);

    // Check untracked page.
    $this->drupalGet('node/' . $untracked_node->id());
    $drupalSettings = $this->getDrupalSettings();
    $this->assertArrayNotHasKey('sharpspring', $drupalSettings);
  }

  /**
   * Tests access checks based on roles for SharpSpring tracking.
   */
  public function testRoleAccess(): void {
    // Create users.
    $admin_role = $this->createAdminRole();
    $regular_role = $this->drupalCreateRole([]);
    $adminUser = $this->drupalCreateUser([], NULL, FALSE, ['roles' => [$admin_role]]);
    $regularUser = $this->drupalCreateUser([], NULL, FALSE, ['roles' => [$regular_role]]);

    // Configure SharpSpring to track only admin users.
    $this->config('sharpspring.settings')
      ->set('visibility_roles', 0)
      ->set('roles', [$admin_role => $admin_role])
      ->save();

    // Check as admin user.
    $this->drupalLogin($adminUser);
    $this->drupalGet('<front>');
    $drupalSettings = $this->getDrupalSettings();
    $this->assertArrayHasKey('sharpspring', $drupalSettings);

    // Check as regular user.
    $this->drupalLogin($regularUser);
    $this->drupalGet('<front>');
    $drupalSettings = $this->getDrupalSettings();
    $this->assertArrayNotHasKey('sharpspring', $drupalSettings);

    // Configure SharpSpring to track all users except admin users.
    $this->config('sharpspring.settings')
      ->set('visibility_roles', 1)
      ->set('roles', [$admin_role => $admin_role])
      ->save();

    // Check as admin user.
    $this->drupalLogin($adminUser);
    $this->drupalGet('<front>');
    $drupalSettings = $this->getDrupalSettings();
    $this->assertArrayNotHasKey('sharpspring', $drupalSettings);

    // Check as regular user.
    $this->drupalLogin($regularUser);
    $this->drupalGet('<front>');
    $drupalSettings = $this->getDrupalSettings();
    $this->assertArrayHasKey('sharpspring', $drupalSettings);
  }

  /**
   * Tests the SharpSpring access hook.
   */
  public function testSharpSpringAccessHook(): void {
    $this->drupalGet('<front>');
    $drupalSettings = $this->getDrupalSettings();
    $this->assertArrayHasKey('sharpspring', $drupalSettings);

    $this->state->set('sharpspring_test.return_forbidden_in_hook_sharpspring_access', TRUE);
    $this->state->set('sharpspring_test.return_forbidden_in_hook_sharpspring_access_alter', FALSE);
    $this->rebuildAll();

    $this->drupalGet('<front>');
    $drupalSettings = $this->getDrupalSettings();
    $this->assertArrayNotHasKey('sharpspring', $drupalSettings);

    $this->state->set('sharpspring_test.return_forbidden_in_hook_sharpspring_access', FALSE);
    $this->state->set('sharpspring_test.return_forbidden_in_hook_sharpspring_access_alter', TRUE);
    $this->rebuildAll();

    $this->drupalGet('<front>');
    $drupalSettings = $this->getDrupalSettings();
    $this->assertArrayNotHasKey('sharpspring', $drupalSettings);
  }

}
