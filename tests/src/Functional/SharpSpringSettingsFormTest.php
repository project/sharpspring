<?php

namespace Drupal\Tests\sharpspring\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the SharpSpring settings form functionality.
 *
 * @group sharpspring
 */
class SharpSpringSettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['sharpspring'];

  /**
   * A user with permission to administer sharpspring.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create and log in our admin user.
    $this->adminUser = $this->drupalCreateUser(['administer sharpspring']);
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests SharpSpring settings form submission.
   */
  public function testSharpSpringSettingsForm() {
    // Test access to the form.
    $this->drupalGet('admin/config/system/sharpspring');
    $this->assertSession()->statusCodeEquals(200);

    // Test form submission.
    $edit = [
      'sharpspring_account' => 'KOI—1234567',
      'sharpspring_domain' => 'koi-123456.sharpspring.com',
      'sharpspring_api_account_id' => 'test_account_id',
      'sharpspring_api_secret_key' => 'test_secret_key',
      'visibility_pages' => '0',
      'pages' => "/node\n/user",
      'visibility_roles' => '0',
      'roles[authenticated]' => 'authenticated',
    ];

    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    // Check if the configuration was saved correctly.
    $config = $this->config('sharpspring.settings');
    $this->assertEquals('KOI-1234567', $config->get('sharpspring_account'));
    $this->assertEquals('koi-123456.sharpspring.com', $config->get('sharpspring_domain'));
    $this->assertEquals('test_account_id', $config->get('sharpspring_api_account_id'));
    $this->assertEquals('test_secret_key', $config->get('sharpspring_api_secret_key'));
    $this->assertEquals(0, $config->get('visibility_pages'));
    $this->assertEquals("/node\n/user", $config->get('pages'));
    $this->assertEquals(0, $config->get('visibility_roles'));
    $this->assertEquals(['authenticated' => 'authenticated'], $config->get('roles'));

    // Verify that the new values are displayed in the form.
    $this->drupalGet('admin/config/system/sharpspring');
    $this->assertSession()->fieldValueEquals('sharpspring_account', 'KOI-1234567');
    $this->assertSession()->fieldValueEquals('sharpspring_domain', 'koi-123456.sharpspring.com');
    $this->assertSession()->fieldValueEquals('sharpspring_api_account_id', 'test_account_id');
    $this->assertSession()->fieldValueEquals('sharpspring_api_secret_key', 'test_secret_key');
    $this->assertSession()->fieldValueEquals('visibility_pages', 0);
    $this->assertSession()->fieldValueEquals('pages', "/node\n/user");
    $this->assertSession()->fieldValueEquals('visibility_roles', 0);
    $this->assertSession()->checkboxChecked('roles[authenticated]');

    // Test form validation.
    $this->submitForm([
      'sharpspring_account' => 'invalid-account',
      'sharpspring_domain' => 'invalid-account',
    ], 'Save configuration');
    $this->assertSession()->pageTextContains('A valid SharpSpring Web Property ID is case sensitive and formatted like KOI-xxxxxxx.');
    $this->assertSession()->pageTextContains('A valid SharpSpring Domain is formatted like koi-XXXXXX.sharpspring.com.');

    // Test access denied for unauthorized user.
    $this->drupalLogout();
    $this->drupalGet('admin/config/system/sharpspring');
    $this->assertSession()->statusCodeEquals(403);
  }

}
